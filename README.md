# Jailbreak

## Development

To clone it and get started:

```bash
git clone git@framagit.org:jailbreak/jailbreak.paris.git
cd jailbreak.paris
npm install
cp .env.example .env
npm run dev
```

Open up [localhost:3000](http://localhost:3000).

## TODO

Inspirations

- http://scopyleft.fr/
- https://delicious-insights.com/
- https://datactivist.coop/fr/
- https://bearstech.com/
- http://ydil.paris/

```
Nous pouvons vous aider à :

Définir et mettre en œuvre une stratégie d’open data
Sensibiliser les acteurs et les décideurs à la culture des données et de leur ouverture
Cartographier vos données et distinguer les données ouvrables
Configurer l’organisation pour l’ouverture, mettre en place des processus internes
Adapter les systèmes d’information et les process à l’ouverture des données
Améliorer la qualité et l’interopérabilité des données ouvertes
Identifier et lever les verrous à la réutilisation par la recherche en sciences sociales et en informatique
Travailler avec les réutilisateurs et mettre en place une stratégie d’open data guidée par la demande
```

Idées

- lister nos expertises techniques par noms de technos (cf https://bearstech.com/)
- logos "communautaires" (april, OSM, etc.)

URLs:

- /projects/dbnomics
- /projects/validata
- /tags/static-site-generator
- /blog/2019/03/dbnomics-and-frictionless-data
