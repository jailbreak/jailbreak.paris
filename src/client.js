import * as sapper from "../__sapper__/client.js"
import { store } from "./store.js"

sapper.start({
	target: document.querySelector("#sapper"),
	store: data => store
})
