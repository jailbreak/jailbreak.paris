import { i18n } from "svelte-i18n"
import { Store } from "svelte/store"
import { defaultLocale, dictionary } from "./i18n.js"

export const store = i18n(new Store(), { dictionary })

store.i18n.setLocale(defaultLocale)
