import { baseUrl, twitterUrl } from "./config.js"

function cleanupArray(array) {
	const filtered = array.filter(Boolean)
	return filtered.length > 0 ? filtered : undefined
}

export const organizationJsonLd = {
	"@context": "https://schema.org",
	"@type": "Organization",
	name: "Jailbreak",
	description: "Free software for free data",
	url: baseUrl,
	logo: `${baseUrl}/jailbreak-logo-square.png`,
	sameAs: cleanupArray([twitterUrl])
}

export function jsonLdScript(data) {
	// Helps including JSON content in the HTML part of a Svelte component.
	return `<script type="application/ld+json">${JSON.stringify(data)}</script>`
}
