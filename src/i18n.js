export const defaultLocale = "fr-FR"

export function fillDictionary(referenceLocale, dictionary) {
	// Take a reference locale and add missing paths to other locales based on it.
	// Mutates `dictionary`.
	for (const locale of Object.keys(dictionary)) {
		if (locale === referenceLocale) {
			continue
		}
		for (const path of Object.keys(dictionary[referenceLocale])) {
			if (!dictionary[locale][path]) {
				dictionary[locale][path] = dictionary[referenceLocale][path]
			}
		}
	}
}

export const dictionary = {
	"en-US": {
		addressCountry: "France",
		addressLocality: "Paris, 14th arrondissement",
		agile: "Agile",
		agileParagraphs: `
			<p>
				Jailbreak works in collaboration with its customers, who are themselves actors throughout the project.
			</p>
			<p>
				It integrates humanity into the heart of work processes, adapting the methodology according to the people.
			</p>
			<p>
				She leads projects from start to end, from design to implementation and deployment.
			</p>
			<p>
				It delivers the product continuously, and priorities are reviewed with the customer after each delivery.
			</p>
		`,
		catchPhrase: "Free software for free data.",
		contactUsPrefix: "Contact us:",
		chatWithUsPrefix: "Chat with us:",
		copyright: "© 2017 – {currentYear} Jailbreak SAS, all rights reserved",
		data: "Data",
		dataParagraphs: `
			<p>
				As data specialists, we take care of processing, cleaning, rectifying and structuring data.
			</p>
			<p>
				We advise local authorities and public administrations that need to work with <em>open data</em> as we know ecosystems, repositories, tools (as well as legal framework in France).
			</p>
			<p>
				We design processing chains, from raw data to usable data (publication, extraction of indicators, dashboards, etc.).
			</p>
		`,
		developer: "Developer",
		devOps: "DevOps",
		devOpsParagraphs: `
			<p>
				We automate and speed-up your applications deployment and guarantee traceability at each step of the process.
			</p>
			<p>
				By adopting a service-centric architecture, we reduce server resources and improve the performance of your applications.
			</p>
			<p>
				We give priority to: <em>containers</em>, GitLab CI/CD (intégration continue), Git for the source code and the data, static websites.
			</p>
			<p>
				We audit your architecture and offer you the most suitable solution to automate its processes.
			</p>
		`,
		involved: "Involved",
		involvedParagraphs: `
			<p>
				Jailbreak is committed to putting the strength of communities at the service of its customers.
			</p>
			<p>
				Jailbreak's added value is to be able to identify these different communities, interact with them to boost their talents.
			</p>
			<p>
				A successful digital transformation project now involves these communities.
			</p>
		`,
		consultant: "Consultant",
		ourCompany: "Our company",
		ourCompanyOpen: "Open",
		ourCompanyOpenParagraphs: `
			<p>
				Jailbreak has openness as DNA.
			</p>
			<p>
				It applies to source code, data, APIs provided by Jailbreak and even in its inner functioning. We carry this value to all domains: public administration, private sector, research, associations...
			</p>
			<p>
				Jailbreak projects are based on standards and software modules developed, supported and maintained by the free software communities and <em>open data</em> initiative.
			</p>
			<p>
				Jailbreak thus reinforces the independence of its customers and guarantees the sustainability and interoperability of its projects.
			</p>
		`,
		ourExpertise: "Our expertise",
		projects: {
			dbnomics: {
				paragraphs: `
					<p>
						DBnomics is the largest free economic database, aggregating hundreds of millions of time series from several dozen sources (INSEE, Eurostat, IMF, WTO, WB...) and making them available via a single API.
					</p>
					<p class="made_for">
						Built for the <a href="https://www.cepremap.fr/" target="_blank">Center for Economic Research and its Applications (CEPREMAP)</a>.
					</p>
				`,
				subTitle: "Helping researchers find the data they need"
			},
			validata: {
				paragraphs: `
					<p>
						Tool to help French local authorities adopt an <em>open data</em> strategy by validating the quality of the data sthey publish.
					</p>
					<p class="made_for">
						Built for <a href="http://opendatafrance.net/" target="_blank">OpenDataFrance</a>.
					</p>
				`,
				subTitle: "Improve quality and interoperability of open datasets"
			},
			observatoire: {
				paragraphs: `
					<p>
					 Dashboard and map to visualize indicators measuring the level of <em>open data</em> activity by French local governments.
					</p>
					<p class="made_for">
					 Built for <a href="http://opendatafrance.net/" target="_blank">OpenDataFrance</a>.
					</p>
				`,
				subTitle: "French Open Data Observatory"
			},
			digitaltransport: {
				paragraphs: `
					<p>
					 Resource Center to foster the production of open data, open source software and shared knowledge to improve transportation in developing countries.
					</p>
					<p class="made_for">
					 Built for the <a href="https://www.afd.fr/" target="_blank">French Development Agency</a> with <a href="http://lafabriquedesmobilites.fr/" target="_blank">la Fabrique des mobilités</a>.
					</p>
				`,
				subTitle: "Mobility Data for Development"
			}
		},
		projectsSectionTitle: "Projects",
		sourceCode: "Source code",
		teamSectionTitle: "Team",
		technicalExpertises: {
			dataProcessing: {
				name: "Data processing",
				paragraphs: `
					<p>
						Databases, indexing engines, validation.
					</p>
				`
			},
			designAndUX: {
				name: 'Web design and <abbr title="User Experience">UX</abbr>',
				paragraphs: `
				<p>
					We attach importance to producing clear, homogeneous and accessible interfaces.
				</p>
				`
			},
			devOpsAndCloud: {
				name: "DevOps and Cloud",
				paragraphs: `
				<p>
					We automate the production chain, from development to deployment.
				</p>
				`
			},
			geo: {
				name: "Cartography on the web",
				paragraphs: `
					<p>
						Our team is passionate about OpenStreetMap: a great way to showcase your data.
					</p>
				`
			},
			languages: {
				name: "Languages",
				paragraphs: `
					<p>
						Selected according to their functionality, ecosystem and performance.
					</p>
				`
			},
			staticSites: {
				name: "Static websites",
				paragraphs: `
					<p>
						Lightweight, fast, money saving.
					</p>
				`
			},
			webApplications: {
				name: "Webapps",
				paragraphs: `
					<p>
						We develop modern web applications and <abbr title="Progressive Web Apps">PWA</abbr>.
					</p>
				`
			}
		},
		technicalExpertiseSectionTitle: "Technical expertise",
		twitterPrefix: "Twitter:",
		webApplications: "Webapps",
		webApplicationsParagraphs: `
			<p>
				We carry out tailor-made developments or integrate existing applications, always respecting Web codes and standards (W3C, UX, SEO, API, micro-services, PWA).
			</p>
			<p>
				We promote the Web as an application platform, on all media.
			</p>
			<p>
				We design state-of-the-art web applications, and modernize your existing applications.
			</p>
			<p>
				We support your digital transformation project, for example when migrating from a proprietary system to an open source solution.
			</p>
		`,
		trainingSectionTitle: "Training",
		trainingParagraphs: `
			<p>We organize training days (or half-days) on your premises.
			We deliver technical training modules dedicated to a developer audience
			(Python, Git, Git, Gitlab CI...) as well as more general information sessions
			(OpenData, Free Software, Agile method, ...)</p>
			<p>We remain attentive to your needs!</p>
		`
	},
	"fr-FR": {
		addressCountry: "France",
		addressLocality: "Paris 14ème arrondissement",
		agile: "Agile",
		agileParagraphs: `
			<p>
				Jailbreak travaille en collaboration avec ses clients, eux-mêmes acteurs tout au long du projet.
			</p>
			<p>
				Elle intègre l'humain au cœur des processus de travail, adaptant la méthodologie en fonction des personnes.
			</p>
			<p>
				Elle mène les projets de bout en bout, depuis la conception jusqu'à la mise en œuvre.
			</p>
			<p>
				Elle livre le produit en continu, et les priorités sont révisées avec le client après chaque livraison.
			</p>
		`,
		catchPhrase: "Des logiciels libres pour des données libres.",
		contactUsPrefix: "Nous contacter :",
		chatWithUsPrefix: "Discuter avec nous :",
		copyright: "© 2017 – {currentYear} Jailbreak SAS, tous droits réservés",
		data: "Données",
		dataParagraphs: `
			<p>
				Spécialistes des données, nous prenons en charge leur traitement, leur valorisation, leur curation et leur structuration.
			</p>
			<p>
				Nous mettons ce savoir-faire au service des collectivités locales et administrations publiques qui pratiquent l'<em>open data</em>, dont nous connaissons les écosystèmes, référentiels, outils ainsi que le cadre légal.
			</p>
			<p>
				Nous concevons des chaînes de traitement, de la donnée brute à la donnée exploitable (publication, calcul d'indicateurs, tableaux de bord, etc.).
			</p>
		`,
		developer: "Développeur",
		devOps: "DevOps",
		devOpsParagraphs: `
			<p>
				Nous automatisons et accélérons le déploiement de vos applications et garantissons la traçabilité à chaque étape du processus.
			</p>
			<p>
				En adoptant une architecture centrée services, nous réduisons les ressources serveur et améliorons les performances de vos applications.
			</p>
			<p>
				Nous privilégions : <em>containers</em>, GitLab CI/CD (intégration continue), Git pour le code et les données, sites statiques.
			</p>
			<p>
				Nous auditons votre architecture et vous proposons la solution la plus adaptée pour automatiser ses processus.
			</p>
		`,
		involved: "Engagée",
		involvedParagraphs: `
			<p>
				Jailbreak s'engage à mettre la force des communautés au service de ses clients.
			</p>
			<p>
				La valeur ajoutée de Jailbreak est de savoir identifier ces différentes communautés, entretenir des liens avec celles-ci et canaliser leurs talents.
			</p>
			<p>
				Bien réussir un projet de transformation numérique passe désormais par ces communautés.
			</p>
		`,
		consultant: "Consultant",
		ourCompany: "Une entreprise",
		ourCompanyOpen: "Ouverte",
		ourCompanyOpenParagraphs: `
			<p>
				Jailbreak a pour valeur centrale l'ouverture.
			</p>
			<p>
				Elle l'applique aux codes sources, aux données, aux APIs, à elle-même, et cela dans toutes les domaines : administration publique, secteur privé, recherche, associatif…
			</p>
			<p>
				Les projets Jailbreak s'appuient sur des standards et des briques développées, portées et maintenues par les communautés du logiciel libre et de l'<em>open data</em>.
			</p>
			<p>
				Jailbreak renforce ainsi l'indépendance de ses clients, garantit la pérennité et l'interopérabilité de ses projets.
			</p>
		`,
		ourExpertise: "Notre expertise",
		projects: {
			dbnomics: {
				paragraphs: `
					<p>
						Aggrège des centaines de millions de séries temporelles provenant de plusieurs dizaines de sources (INSEE, Eurostat, FMI, WTO, WB...) et les met à disposition via une API unique.
					</p>
					<p class="made_for">
						Réalisé pour le <a href="https://www.cepremap.fr/" target="_blank">Centre pour la recherche économique et ses applications (CEPREMAP)</a>.
					</p>
				`,
				subTitle: "Aider les chercheur‧se‧s à trouver les données utiles à leur travail"
			},
			validata: {
				paragraphs: `
					<p>
						Outil pour aider les collectivités françaises à adopter une stratégie <em>open data</em> en validant la qualité des jeux de données qu'elles publient.
					</p>
					<p class="made_for">
						Réalisé pour <a href="http://opendatafrance.net/" target="_blank">OpenDataFrance</a>.
					</p>
				`,
				subTitle: "Améliorer la qualité et l'interopérabilité des jeux de données ouverts"
			},
			observatoire: {
				paragraphs: `
					<p>
					 Tableau de bord et cartographie permettant la visualisation d'indicateurs de l'activité des collectivités françaises en matière de publication de données publiques.
					</p>
					<p class="made_for">
					 Réalisé pour <a href="http://opendatafrance.net/" target="_blank">OpenDataFrance</a>.
					</p>
				`,
				subTitle: "Faire un état des lieux des données ouvertes en France"
			},
			digitaltransport: {
				paragraphs: `
					<p>
					 Centre de ressources pour favoriser la production de données ouvertes, logiciels libres et connaissances partagées, afin d’améliorer l’information voyageurs dans les pays en développement.
					</p>
					<p class="made_for">
					 Réalisé pour l'<a href="https://www.afd.fr/" target="_blank">Agence française de développement</a> avec <a href="http://lafabriquedesmobilites.fr/" target="_blank">la Fabrique des mobilités</a>.
					</p>
				`,
				subTitle: "Ouvrir les données de mobilité pour les pays en développement"
			}
		},
		projectsSectionTitle: "Nos réalisations",
		sourceCode: "Code source",
		teamSectionTitle: "L'équipe",
		technicalExpertises: {
			dataProcessing: {
				name: "Traitement de données",
				paragraphs: `
					<p>
						Bases de données, moteurs d'indexation, validation.
					</p>
				`
			},
			designAndUX: {
				name: 'Design et <abbr title="User Experience">UX</abbr>',
				paragraphs: `
				<p>
					Nous accordons de l'importance à produire des interfaces claires, homogènes et accessibles.
				</p>
				`
			},
			devOpsAndCloud: {
				name: "DevOps et Cloud",
				paragraphs: `
				<p>
					Nous automatisons la chaîne de production, du développement jusqu'au déploiement.
				</p>
				`
			},
			geo: {
				name: "Cartographie Web",
				paragraphs: `
					<p>
						Notre équipe est passionnée par OpenStreetMap : une excellente façon de mettre en valeur vos données.
					</p>
				`
			},
			languages: {
				name: "Langages",
				paragraphs: `
					<p>
						Sélectionnés en fonction de leurs fonctionnalités, leur écosystème et leurs performances.
					</p>
				`
			},
			staticSites: {
				name: "Sites statiques",
				paragraphs: `
					<p>
						Légers, rapides, économiques.
					</p>
				`
			},
			webApplications: {
				name: "Applications Web",
				paragraphs: `
					<p>
						Nous réalisons des applications Web modernes et des <abbr title="Progressive Web Apps">PWA</abbr>.
					</p>
				`
			}
		},
		technicalExpertiseSectionTitle: "Expertise technique",
		twitterPrefix: "Twitter :",
		webApplications: "Applications Web",
		webApplicationsParagraphs: `
			<p>
				Nous réalisons des développements sur-mesure ou intégrons des applications existantes, toujours en respectant les codes et les standards du Web (W3C, UX, SEO, API, micro-services, PWA).
			</p>
			<p>
				Nous favorisons le Web comme plateforme applicative, sur tous les supports.
			</p>
			<p>
				Nous concevons des applications Web à l'état de l'art, et modernisons vos applications existantes.
			</p>
			<p>
				Nous accompagnons votre projet de transformation numérique, par exemple à l'occasion de la migration d'un système propriétaire vers une solution libre.
			</p>
		`,
		trainingSectionTitle: "Formation",
		trainingParagraphs: `<p>Nous organisons des journées (ou demi-journées) de formation
		dans vos locaux. Nous dispensons autant des modules de formation technique à destination
		des développeurs (Python, Git, Gitlab CI...) que des séances d'information plus générales
		(OpenData, logiciels libres, Conduite de projet en méthode agile, ..)</p>
		<p>Nous restons à votre disposition pour tout besoin de formation.</p>`
	}
}

fillDictionary("fr-FR", dictionary)

export function isLocale(s) {
	const locales = Object.keys(dictionary)
	return locales.includes(s)
}

export const localesNames = [
	["fr-FR", "FR"], //
	["en-US", "EN"]
]
